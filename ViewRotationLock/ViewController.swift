//
//  ViewController.swift
//  ViewRotationLock
//
//  Created by Nicholas Lau on 8/9/2018.
//  Copyright © 2018 Nicholas Lau. All rights reserved.
//

import UIKit
import PureLayout

class ViewController: UIViewController {

    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var freeImageView: UIImageView!
    
    private var prevDeviceOrientation: UIDeviceOrientation!
    
    private var smallRect: CGRect { return CGRect(x: 5, y: 50, width: 300 , height: CGFloat(300 * 9.0 / 16.0))}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prevDeviceOrientation = UIDevice.current.orientation
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let green = UIView()
        green.backgroundColor = UIColor.green
        green.alpha = 0.4
        freeImageView.addSubview(green)
        freeImageView.contentMode = .scaleAspectFit
        green.autoPinEdgesToSuperviewEdges()
        
        lockImageView.frame = view.bounds
        freeImageView.frame = smallRect
        
        prevDeviceOrientation = UIDevice.current.orientation
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("Will Rotate")
        
        let portraitToLandscape = prevDeviceOrientation.isPortrait && UIDevice.current.orientation.isLandscape
        
        prevDeviceOrientation = UIDevice.current.orientation
        
        coordinator.animate(alongsideTransition: { [unowned self] (context) in
            print("Rotating")
            
            if UIDevice.current.orientation.isPortrait {
                // Return to normal
                self.lockImageView.transform = CGAffineTransform.identity
                self.lockImageView.center = self.view.center
                
                self.freeImageView.frame = self.smallRect
                self.freeImageView.layoutIfNeeded()
            }
            else if portraitToLandscape {
                // Lock bg view
                self.lockImageView.center = context.containerView.center
                self.lockImageView.transform = context.targetTransform.inverted()
                
                // Rotate and fullscreen free view
                self.freeImageView.frame = self.view.bounds
                self.freeImageView.layoutIfNeeded()
            }
            
        }) { (context) in
            print("Did Rotate")
        }
        super.viewWillTransition(to: size, with: coordinator)
    }

}
